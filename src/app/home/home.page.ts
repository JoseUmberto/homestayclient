import { Component } from '@angular/core';
import { HttpserviceService } from './httpservice.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  body: object = {};
  constructor(private http: HttpserviceService){}

  onClickMe(signin: string, passwrod: string) {
    this.body = {
      'signin': signin,
      'password': passwrod
    };
    console.log("OI", this.body)
    this.http.sendLogin(this.body).subscribe((string)=>{
      console.log(string);
    })
  }
}
